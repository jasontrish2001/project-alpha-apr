from django.db import models

# Create your models here.


class Project(models.Model):
    def __init__(self, name, description, members):
        self.name = models.CharField(max_length=200)
        self.description = models.TextField()
        self.members = models.ForeignKey(
            on_delete=models.CASCADE, auth_USER=models.AuthUser
        )

    def __str__(self):
        return self.name
